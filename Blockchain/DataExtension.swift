//
//  DataExtension.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 2/1/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation

extension Data {
    public func sha256Hash() -> Data {
        let transform = SecDigestTransformCreate(kSecDigestSHA2, 256, nil)
        SecTransformSetAttribute(transform, kSecTransformInputAttributeName, self as CFTypeRef, nil)
        return SecTransformExecute(transform, nil) as! Data
    }
}
