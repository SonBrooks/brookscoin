//
//  main.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 1/30/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation

// Fakify Transactions
var names : [String] = ["Andrew", "Tim", "Amy", "Jerry", "Becky", "Jeremy"]
var amountToSend : Int = -1
var currentTransactions = [""]

func createFakeTransactions() -> [Transaction] {
    let numberOfNewTransactions: Int = Int(arc4random_uniform(10))
    var newTransactions: [Transaction] = []
    var currentSender: String = ""
    var currentReceiver: String = ""
    
    for _ in 0...numberOfNewTransactions {
        currentSender = names[Int(arc4random_uniform(UInt32(names.count)))]
        currentReceiver = names[Int(arc4random_uniform(UInt32(names.count)))]
        let transactionAmount = arc4random_uniform(1000)
        let newTransaction: Transaction = Transaction(sender: currentSender, recipient: currentReceiver, amountTransfered: Double(transactionAmount))
        newTransactions.append(newTransaction)
    }
    
    return newTransactions
}

for difficulty in 1...1 {
    let brooksCoin = Blockchain()
    brooksCoin.difficulty = difficulty
    let startTime = DispatchTime.now()
    
    for i in 0...250 {
        let newTransactions = createFakeTransactions()
        
        brooksCoin.pendingTransactions.append(contentsOf: newTransactions)
        brooksCoin.minePendingTransactions(miningRewardAddress: "Sonny")
        
        print("\n*-*- BLOCK MINED! -*-*")
        print("*---- BLOCK \(i) ----*")
        print("Created At: \(brooksCoin.getLatestBlock().getTimestamp())")
        print("Block Hash: \(brooksCoin.getLatestBlock().getBlockHash())")
        print("Previous Block Hash: \(brooksCoin.getLatestBlock().getPreviousHash())")
        print("Block Transactions: \(brooksCoin.getLatestBlock().getTransactions().count) Transactions made.")
        print("*---- END BLOCK \(i) ----*\n")
    }

    print("\n**************************\n Verifying Blockchain...\n**************************")
    if brooksCoin.isChainValid() {
        print("Blockchain is valid!")
    } else {
        print("Your blockchain was incorretly implemented.")
    }
    
    print("\nDifficulty \(difficulty) took (\(Double(DispatchTime.now().uptimeNanoseconds - startTime.uptimeNanoseconds) * pow(10, -9))s) to mine 250 Blocks")
    
    print("Andrew has \(brooksCoin.checkBalanceOfAddress(address: "Andrew"))")
}



