//
//  Blockchain.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 1/31/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation

class Blockchain {
    var chain: [Block]
    var difficulty: Int = 2
    var pendingTransactions: [Transaction] = []
    var miningReward: Double = 2.0
    
    init() {
        self.chain = []
        self.chain.append(self.createGenesisBlock())
    }
    
    private func createGenesisBlock() -> Block {
        let genesis: Block = Block(previousHash: "0x00", transactions: [Transaction(sender: "GENESIS", recipient: "GENESIS", amountTransfered: 0.0)])
        genesis.setNonce(previousNonce: 0)
        return genesis
    }
    
    func isChainValid() -> Bool {
        for i in 1..<self.chain.count{
            let currentBlock = self.chain[i]
            let previousBlock = self.chain[i - 1]
            
            if currentBlock.generateHashForBlock() != currentBlock.getBlockHash() {
                return false
            }
            if currentBlock.getPreviousHash() != previousBlock.getBlockHash() {
                return false
            }
        }
        return true
    }
    
    func getLatestBlock() -> Block {
        return self.chain.last!
    }
    
    func minePendingTransactions(miningRewardAddress: String) {
        let block = Block(previousHash: (self.chain.last?.getBlockHash())!, transactions: self.pendingTransactions)
        block.mineBlock(difficulty: self.difficulty)
        
        self.chain.append(block)
        
        self.pendingTransactions = []
        self.pendingTransactions.append(Transaction(sender: "BLOCKCHAIN REWARD", recipient: miningRewardAddress, amountTransfered: self.miningReward))
    }
    
    func checkBalanceOfAddress(address: String) -> Double {
        var balance = 0.00
        
        for block in self.chain {
            for transaction in block.getTransactions() {
                if transaction.sender == address {
                    balance -= transaction.amountTransfered
                } else if transaction.recipient == address {
                    balance += transaction.amountTransfered
                }
            }
        }
        
        return balance
    }
    
}
