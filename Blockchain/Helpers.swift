//
//  Helpers.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 2/1/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation

public func getTodayString() -> String {
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    
    let year = components.year
    let month = components.month
    let day = components.day
    let hour = components.hour
    let minute = components.minute
    let second = components.second
    
    let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
    
    return today_string
}
