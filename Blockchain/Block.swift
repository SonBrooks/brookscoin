//
//  Block.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 1/30/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation


class Block {
    private var previousHash: String = ""
    private var timestamp: String
    private var transactions: [Transaction] = []
    private var blockHash: String = ""
    private var nonce: Int = 0

    init(previousHash: String, transactions: [Transaction]) {
        self.previousHash = previousHash
        self.transactions = transactions
        self.timestamp = ""
        self.blockHash = "GENESIS"
    }
    
    func generateHashForBlock() -> String {
        var hash: String = String((transactions.last?.amountTransfered.hashValue)!) + " " + String((transactions.first?.amountTransfered.hashValue)!)
        hash += "\(previousHash.hashValue)\(self.nonce)"
        hash = (hash.data(using: .utf8)?.sha256Hash().base64EncodedString())!
        return hash
    }
    
    func getTimestamp() -> String {
        return self.timestamp
    }
    
    func setTimestamp() {
        self.timestamp = getTodayString()
    }
    
    func getBlockHash() -> String {
        return self.blockHash
    }
    
    func getTransactions() -> [Transaction] {
        return self.transactions
    }
    
    
    // NONCE
    func getNonce() -> Int {
        return self.nonce
    }
    
    func setNonce(previousNonce: Int) {
        self.nonce = previousNonce
    }

    // HASH
    func getPreviousHash() -> String {
        return self.previousHash
    }
    
    
    func mineBlock(difficulty: Int) {
        let range = self.blockHash.index(self.blockHash.startIndex, offsetBy: difficulty)
        let solveString = String(repeating: "0", count: difficulty)

        while(String(self.blockHash[..<range]) != solveString) {
            self.nonce += 1
            self.blockHash = self.generateHashForBlock()
        }
        
        self.setTimestamp()
    }
}
