//
//  Transaction.swift
//  Blockchain
//
//  Created by Andrew Budziszek on 1/31/18.
//  Copyright © 2018 Andrew Budziszek. All rights reserved.
//

import Foundation

class Transaction {
    let sender: String
    let recipient: String
    let amountTransfered: Double
    
    init(sender: String, recipient: String, amountTransfered: Double) {
        self.sender = sender
        self.recipient = recipient
        self.amountTransfered = amountTransfered
    }
}
